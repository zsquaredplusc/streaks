include("FastCombos.jl")
import Combinatorics.fibonaccinum

@fastmath function streak(n::Int64, r::Int64, p::Float64, s=1, exactlength=false)  # P(at least one streak of length ≥r in n trials with P(success)=p)
    if s>1 return multistreak(n,r,s,p) #,exactlength)
    elseif exactlength return exactlenstrk(n,r,p)
    elseif n<r || p≤0.0 return 0
    elseif r<1 || p≥1.0 return 1
    elseif n≤2r return sievstreak(n,r,p)
    elseif r==1 return 1.0-(1.0-p)^n
    elseif r==2
        if n>13 return round(streakof2(n,p), digits=10)  # Not as precise with smaller n.
        elseif p==.5 && n>28 return Float64(1.0-fibonaccinum(n+2)/BigInt(2)^n)
        else return villarino(n,r,p) end
    else
        matrix = false
        if r==3 matrix=(n>72)
        elseif r==4 matrix=(n>95)
        elseif r==5 matrix=(n>114)
        elseif r==6 matrix=(n>129)
        elseif r==7 matrix=(n>153)
        elseif r==8 matrix=(n>191)
        elseif r==9 matrix=(n>230)
        elseif r==10 matrix=(n>255)
        elseif r==11 matrix=(n>287)
        elseif r==12 matrix=(n>303)
        elseif r==13 matrix=(n>320)
        elseif r==14 matrix=(n>349)
        else matrix = (n > 23*(r-10)+255) end
        if matrix return matrixstreak(n,r,p)
        else return villarino(n,r,p) end
    end
end
@fastmath @inbounds function multistreak(n::Int64, r::Int64, s::Int64, p::Float64) # P(at least s disjoint streaks of length ≥r)
    begin q=1-p;  w=s*r;  nmin=(w+s-1) end
    if s<2 return streak(n,r,p)
    elseif r<1
        if r==0 return multistreak(n,1,s,q)
        else throw(DomainError(r,"r cannot be negative")) end
    elseif n<nmin || !(0<p<1) return 0
    elseif n==nmin return p^w * q^(s-1)
    elseif r>1 || (n>9 && (n>4*nmin/3 || s<4))  # 4/3 nmin is a rough opti rule.
        begin m= r+1;  C= fill(0.0,m,m);  T= fill(0.0, nmin+1, nmin+1) end
        begin C[1:2,1]=[p,p];  C[2:m, m]=fill(q,r) end
        for j=2:r begin C[j+1,j]=p end end
        for k=0:s-1 begin T[k*m+1:(k+1)*m, k*m+1:(k+1)*m] = C end end
        for k=1:s-1 begin T[k*m+1, k*m]=q end end
        T[1,1]=1.0
        return (T^n)[1+nmin,1]
    else
        if (s>2 || n<7) return gapstreak(n,r,s,p)
        else # Split into two steps: a transition matrix for the first streak, then a single-streak problem.
            probsum = 0.0
            begin A=fill(0.0,r+2,r+2);  A[1:2,1]=[1.0,q];  A[2:3,2]=[p,p];  A[3:r+2, r+2]=fill(q,r) end
            begin for j=3:r+1 (A[j+1,j]=p) end; AEXP=A^r end
            for t=r+1:n-r begin probsum+= AEXP[r+2,2]*q*streak(n-t,r,p);  AEXP*=A end end
            return probsum
        end
    end
end
@fastmath function exactlenstrk(n::Int64, r::Int64, p::Float64) #, s=1)
    #if s>1 return multistreak(n,r,s,p,true)
    if n<r || (p≤0.0 && r>0) || (p≥1.0 && r<n) return 0
    elseif !(0<p<1) return 1
    elseif r==0 return 1.0-p^n
    elseif n≤2r return sievstreak(n,r,p,true)
    else return matrixstreak(n,r,p,true) end
end
@fastmath @inbounds function matrixstreak(n::Int64, r::Int64, p::Float64, exactlength=false)
    if r>0
        q = 1-p
        if !exactlength
            begin T=fill(0.0,r+1,r+1);  T[1:2,1]=[1.0,p];  T[2:r+1, r+1]=fill(q,r) end
            for j=2:r begin T[j+1,j]=p end end
            return (T^n)[r+1,1]
        else  # There are two extra absorbing states, placed at top/bot rows & L/R cols; the 0-state is one from the bottom.
            T = fill(0.0, r+3, r+3)
            begin T[1:2,1]=[1.0,q];  T[3:r+3, r+2]=fill(q,r+1);  T[2,r+3]=p; T[r+3,r+3]=p end
            for j=2:r+1 begin T[j+1,j]=p end end
            return sum((T^n)[r+2,1:2])  # If r-in-a-row gets completed on the last trial, that counts.
        end
    elseif r==0
        if exactlength return 0
        else return 1 end
    else throw(DomainError(r,"r cannot be negative")) end
end
@fastmath function villarino(n::Int64, r::Int64, p::Float64) # Villarino (2008) Eq#5
    return 1.0+(p^r)*strkBeta(n-r,r,p)-strkBeta(n,r,p)
end
@fastmath function strkBeta(n::Int64, r::Int64, p::Float64) # Villarino (2008) Eq#4
    begin c=(1-p)*p^r;  cexp, probsum = 1.0, 1.0  end
    for k=1:floor(Int64, n/(r+1))
        cexp *= -1.0*c
        probsum += cexp*combo(n-k*r, k)
    end
    return probsum
end
@fastmath function sievstreak(n::Int64, r::Int64, p::Float64, exactlength=false)
    if n≤2r
        if !exactlength return (n+1-r)*p^r - (n-r)*p^(r+1)
        else return (n+1-r)*p^r - 2*(n-r)*p^(r+1) + (n-r-1)*p^(r+2) end
    else throw(DomainError(n/r, "n cannot be larger than 2r")) end
end
@fastmath function streakof2(n::Int64, p::Float64)  # Accurate approximation for P(at least one streak of length ≥2)
    begin w=p^2;  q= sqrt((3p+1)*(1-p)) end
    return 1+((w-2p-1)*q/(3p+1)+(w-1))/2 * ((q-p+1)/2)^(n-2)  # Shoutout to redditor CapaneusPrime
end
@fastmath function gapstreak(n::Int64, r::Int64, s::Int64, p::Float64)
    begin q=1-p;  gmin=s-1;  probsum=0.0  end
    for k= s*r : n-gmin
        begin perms=0; gmax= min(k-1,n-k) end
        if (gmax+2-s)≥gmin for g=gmin:gmax begin perms += gappers(n,k,g) end end
        else
            for g=0:gmin-1 begin perms += gappers(n,k,g) end end
            begin total=combo(n,k);  perms = total-perms end
        end
        probsum += perms * p^k * q^(n-k)
    end
    return probsum
end
@fastmath function gappers(n::Int64, k::Int64, g::Int64)  # N(binary perms of symbol appearing k times, such that the # of dividers is g)
    off = n-k
    if g==0 return off+1
    elseif g==off return combo(k-1, g)
    elseif g>off return 0
    else return combo(k-1, g) * combo(off+1, g+1) end
end
@fastmath function streak2perm(n::Int64, k::Int64)  # N(binary perms of symbol appearing k times, such that there are ≥1 streaks of Len≥2)
    if k≥2
        total = combo(n,k)
        if n-k < k-1 return total
        else return total - combo(n+1-k, k) end
    else return 0 end
end
@fastmath function streak2permprob(n::Int64, k::Int64)  # P(at least one streak of Len≥2 | exactly k successes)
    if k≥2
        if n-k<k-1 return 1
        else return 1.0 - combo(n+1-k, k)/combo(n,k) end
    else return 0 end
end
