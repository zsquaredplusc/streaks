@fastmath function smallcombo(n::Int64, r::Int8)  # Intended to be called by combo(); no bounds check or n-vs-r comparison.
    perms = 1
    @simd for j=n+1-r:n begin perms*=j end end
    return round(Int64, perms/factorial(r))
end
@fastmath function choose(n::Int64, r::Int64, repetition=false)
    if repetition return combo(n+r-1, r)
    else return combo(n,r) end
end
@fastmath function combo(n::Int64, r::Int64)  # Picks the appropriate combo function to use based on n and r.
    if n<0 || r<0 throw(DomainError(min(n,r),"n and r cannot be negative"))
    elseif r>n return 0
    elseif (r>n/2) r=n-r end
    sc,six4 = false,false
    if r>20
        if r==21 six4=(n<80)
        elseif r==22 six4=(n<77)
        elseif r==23 six4=(n<75)
        elseif r==24 six4=(n<73)
        elseif r==25 six4=(n<71)
        elseif r==26 six4=(n<70)
        elseif r==27 six4=(n<69)
        elseif r<30 six4=(n<68)
        elseif r==30 six4=(n<67) end
    elseif r>18
        if n<21 sc=true
        elseif r==19 six4=(n<89)
        else six4 = (n<84) end
    elseif r>16
        if n<22 sc=true
        elseif r==17 six4=(n<102)
        else six4 = (n<95) end
    elseif r==16 begin n<24 ? sc=true : six4=(n<112) end
    elseif r==15 begin n<26 ? sc=true : six4=(n<126) end
    elseif r==14 begin n<30 ? sc=true : six4=(n<144) end
    elseif r==13 begin n<36 ? sc=true : six4=(n<170) end
    elseif r==12 begin n<44 ? sc=true : six4=(n<207) end
    elseif r==11 begin n<59 ? sc=true : six4=(n<266) end
    elseif r==10 begin n<84 ? sc=true : six4=(n<362) end
    elseif r==9 begin n<133 ? sc=true : six4=(n<535) end
    elseif r==8 begin n<239 ? sc=true : six4=(n<888) end
    elseif r==7 begin n<518 ? sc=true : six4=(n<1734) end
    elseif r==6 begin n<1451 ? sc=true : six4=(n<4338) end
    elseif r==5 begin n<6211 ? sc=true : six4=(n<16176) end
    elseif r==4 begin n<55111 ? sc=true : six4=(n<121978) end
    elseif r==3 begin n<2097154 ? sc=true : six4=(n<3810780) end
    elseif r==2 begin n<3037000501 ? sc=true : six4=(n<4294967297) end
    elseif r==1 return n
    elseif r==0 return 1 end
    if sc return smallcombo(n,Int8(r))
    elseif six4 return binomial(n,r)
    else return binomial(BigInt(n),BigInt(r)) end
end
